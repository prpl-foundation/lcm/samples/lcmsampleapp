#!/bin/sh
name="lcmsampleapp"

pidfile="/var/run/${name}.pid"

case $1 in
    start|boot)
        if [ -s ${pidfile} ]; then
            if [ -d "/proc/$(cat ${pidfile})/fdinfo" ]; then
                echo "${name}is already started"
                exit 0
            fi
            rm ${pidfile}
        fi
        ${name} -D
        ;;
    stop|shutdown)
        if [ -f ${pidfile} ]; then
            if [ -d "/proc/$(cat ${pidfile})/fdinfo" ]; then
                kill `cat ${pidfile}`
            fi
            rm ${pidfile}
        fi
        killall ${name}
        ;;
    debuginfo)
        if [ -s ${pidfile} ] && [ -d "/proc/$(cat ${pidfile})/fdinfo" ]; then
            ubus-cli "LCMSampleApp.?"
        else
            echo "${name} is not running"
        fi
        ;;
    restart)
        $0 stop
        $0 start
        ;;
    log)
        echo "TODO log LCMSampleApp client"
        ;;
    *)
        echo "Usage : $0 [start|boot|stop|debuginfo]"
        ;;
esac
