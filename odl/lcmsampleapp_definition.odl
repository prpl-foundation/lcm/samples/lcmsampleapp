%define {
    object LCMSampleApp {
        event "EVENT:LCM:SAMPLE!";
        /*
            Dummy command, it will return the returnvalue.
            The returnvalue is optional, and in that case set to true

            @param returnvalue boolean value to return

            @return
            returns the returnvalue
        */
        bool dummycmd(%in bool returnvalue = true);

        /*
            Will trigger a global event/notification

            @param message to add to event so you can identify the event

            @return
            returns true when it can issue the event
        */
        bool triggerglobalevent(%in string message = "Identify --> No message");

        %persistent uint32 DummyGlobalInt = 0;

        /*
            Sets the value of the DummyGlobalInt.
            Will return the value that is set.

            @param value value to set DummyGlobalInt to

            @return
            returns the value that is set
        */
        uint32 setDummyGlobalInt(%in %mandatory uint32 value);

        /*
            Get the value of the DummyGlobalInt.
            Will return the value that is set.

            @return
            returns the value of DummyGlobalInt
        */
        uint32 getDummyGlobalInt();

        %persistent string DummyGlobalString = "";

        /*
            Sets the value of the DummyGlobalString.
            Will return the value that is set.

            @param value value to set DummyGlobalString to

            @return
            returns the value that is set
        */
        string setDummyGlobalString(%in %mandatory string value);

        /*
            Get the value of DummyGlobalString.
            Will return the value that is set.

            @return
            returns the value that of DummyGlobalString
        */
        string getDummyGlobalString();

        /*
            Deletes the multi instance objects.

            When "force" is set to true, the retained instances will be
            deleted as well.

            @param force when true also delete retained instances

            @return 
            the number of instances deleted
        */
        uint64 clearDummyInstances(%in bool force = false);

        /*
            Add entry to multi instance objects.

            When "retain" is set to true, the instance will be retained on clear()

            @param key the string of the key you want to store
            @param value the string of the value you want to store corresponding to the key
            @param retain when true the instance will be retained on clear

            @return 
            the number of instances deleted
        */
        bool addDummyInstanceEntry(%in %mandatory string key, %in %mandatory string value, %in bool retain = false);

        /*
            Get Value based on Key from the multi instance objects.

            @param key the string of the key you want to get the value from

            @return
            the value of the corresponding key
        */
        string getDummyInstanceValue(%in %mandatory string key);

        /*
            A test method with output arguments.

            @param message A "Hello world" message
            @param data a hash table with a few elements
        */
        uint32 test_out_args(%out string message, %out htable data);

        %persistent object DummyMultiInstance[] {
            counted with NumberOfDummyMultiInstances;

            %persistent %unique %key string Key {
                on action validate call check_maximum_length 64;
            }

            %persistent string Value {
                on action validate call check_maximum_length 256;
            }

            /*
                Can it be deleted automatically.

                When this parameter is set to "true", the instance will not be
                automatically deleted.
            */
            %persistent bool Retain = false;
        }
    }
}

